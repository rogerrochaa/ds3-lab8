package com.example.migrationapi.dtos;

import java.math.BigDecimal;

public record ProductRequestDto(String name, Double value, Integer quantity) { }
