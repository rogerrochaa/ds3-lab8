package com.example.migrationapi.controllers;

import com.example.migrationapi.dtos.ProductRequestDto;
import com.example.migrationapi.models.ProductMongo;
import com.example.migrationapi.services.ProductMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products-mongo")
public class ProductMongoController {

    @Autowired
    private ProductMongoService service;

    @GetMapping
    public ResponseEntity<List<ProductMongo>> getAllProducts() {
        return ResponseEntity.status(HttpStatus.OK).body(service.getAllProducts());
    }
    @PutMapping("/{id}")
    public ResponseEntity<ProductMongo> update(@PathVariable String id, @RequestBody ProductRequestDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(service.updateProduct(id, dto));
    }
}
