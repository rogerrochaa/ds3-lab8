package com.example.migrationapi.controllers;

import com.example.migrationapi.dtos.ProductRequestDto;
import com.example.migrationapi.models.ProductModel;
import com.example.migrationapi.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductRequestDto dto) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.createProduct(dto));
    }

    @GetMapping
    public ResponseEntity<List<ProductModel>> getAllProducts() {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getAllProducts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductModel> getProductById(@PathVariable UUID id) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getProductById(id));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable UUID id) {
        productService.deleteProduct(id);
        return ResponseEntity.status(HttpStatus.OK).body("Product deleted successfully");
    }
    @PutMapping("/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductRequestDto dto, @PathVariable UUID id) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.updateProduct(id, dto));
    }
}
