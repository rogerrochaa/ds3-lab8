package com.example.migrationapi.repositories;

import com.example.migrationapi.models.ProductMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductMongoRepository extends MongoRepository<ProductMongo, String> {
}
