package com.example.migrationapi.models;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="TB_PRODUCTS")
public class ProductModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name = "NAME_PRODUCT")
    private String name;

    @Column(name = "VALUE_PRODUCT")
    private Double value;

    @Column(name = "QNT_PRODUCT")
    private Integer quantity;

    @Column(name = "DT_CREATED_AT")
    @CreationTimestamp
    private Instant createdAt;

    @Column(name = "DT_UPDATED_AT")
    @UpdateTimestamp
    private Instant updatedAt;

    public UUID getIdProduct() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }
}
