package com.example.migrationapi.models;

import org.springframework.data.mongodb.core.mapping.Field;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.time.Instant;


@Document(collection = "productMongo")
@Data
@AllArgsConstructor
public class ProductMongo {
    @MongoId
    private String id;

    @Field(name = "NAME_PRODUCT")
    private String name;

    @Field(name = "VALUE_PRODUCT")
    private Double value;

    @Field(name = "QNT_PRODUCT")
    private Integer quantity;

    @Field(name = "DT_CREATED_AT")
    private Instant createdAt;
    @Field(name = "DT_UPDATED_AT")
    private Instant updatedAt;
}
