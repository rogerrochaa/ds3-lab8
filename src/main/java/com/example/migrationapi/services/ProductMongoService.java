package com.example.migrationapi.services;

import com.example.migrationapi.dtos.ProductRequestDto;
import com.example.migrationapi.models.ProductMongo;
import com.example.migrationapi.repositories.ProductMongoRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class ProductMongoService {

    @Autowired
    private ProductMongoRepository repository;

    public List<ProductMongo> getAllProducts() {
        return repository.findAll();
    }

    public ProductMongo updateProduct(String id, ProductRequestDto dto) {
        ProductMongo productMongo = repository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found"));
        BeanUtils.copyProperties(dto, productMongo);
        return repository.save(productMongo);
    }
}
