package com.example.migrationapi.services;

import com.example.migrationapi.dtos.ProductRequestDto;
import com.example.migrationapi.models.ProductModel;
import com.example.migrationapi.repositories.ProductRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public ProductModel createProduct(ProductRequestDto dto) {
        ProductModel product = new ProductModel();
        BeanUtils.copyProperties(dto, product);
        return productRepository.save(product);
    }

    public List<ProductModel> getAllProducts() {
        return productRepository.findAll();
    }
    public ProductModel getProductById(UUID id) {
        return productRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found"));
    }
    public void deleteProduct(UUID id) {
        var product = getProductById(id);
        productRepository.delete(product);
    }
    public ProductModel updateProduct(UUID id, ProductRequestDto dto) {
        var product = getProductById(id);

        BeanUtils.copyProperties(dto, product);
        return productRepository.save(product);
    }


}
