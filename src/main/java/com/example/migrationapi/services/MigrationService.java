package com.example.migrationapi.services;

import com.example.migrationapi.models.ProductModel;
import com.example.migrationapi.models.ProductMongo;
import com.example.migrationapi.repositories.ProductMongoRepository;
import com.example.migrationapi.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MigrationService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMongoRepository productMongoRepository;

    public void migrate() {
        List<ProductModel> productElements = productRepository.findAll();
        for (ProductModel productElement : productElements) {
            ProductMongo productMongo = new ProductMongo(
                    productElement.getIdProduct().toString(),
                    productElement.getName(),
                    productElement.getValue(),
                    productElement.getQuantity(),
                    productElement.getCreatedAt(),
                    productElement.getUpdatedAt());

            productMongoRepository.save(productMongo);
        }
    }
}
